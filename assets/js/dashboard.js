var diaObj =  !{JSON.stringify(diaObj[0])};

$(document).ready(function(){

	inicializarMaterial();
	
	inicializarGraficas();

	inicializarSockets();

	var $chart = $('#graph-dia');

	var $toolTip = $chart
		.append('<div class="tooltip"></div>')
		.find('.tooltip')
		.hide();
	
	$chart.on('mouseenter', '.ct-point', function() {
		var $point = $(this),
		value = $point.attr('ct:value'),
		seriesName = $point.parent().attr('ct:series-name');
		$toolTip.html(seriesName + '<br>' + value).show();
	});

	$chart.on('mouseleave', '.ct-point', function() {
		$toolTip.hide();
	});

	$chart.on('mousemove', function(event) {
		$toolTip.css({
			left: (event.offsetX || event.originalEvent.layerX) - $toolTip.width() / 2 - 10,
			top: (event.offsetY || event.originalEvent.layerY) - $toolTip.height() - 40
		});
	});

});

function inicializarMaterial(){
	$(".button-collapse").sideNav();
	$('.datepicker').pickadate({
		selectMonths: true,
		selectYears: 5,
		weekdaysLetter: [ 'D', 'L', 'M', 'M', 'J', 'V', 'S' ],
		today: 'Hoy',
		clear: 'Limpiar',
		close: 'Aceptar',
		onClose: function(){
		   $(document.activeElement).blur()
		}
	});
}

function inicializarGraficas(){
	graphHora();
	graphMax();

	$("#visitantes-actuales").html(diaObj.actuales)
	$("#visitantes-totales").html(diaObj.entrada)
}

function inicializarSockets(){
	
	io.socket.on("entrada", actualizarSockets);
	io.socket.get("/entrada", function(resData, jwres) {})
	io.socket.on("salida", actualizarSockets);
	io.socket.get("/salida", function(resData, jwres) {})

}

function actualizarSockets(){
	io.socket.get('/dia?dia='+diaObj.dia, function(resData, jwres){
		diaObj = resData[0];
		inicializarGraficas();
	});
}

function graphHora(){
	var data = {};
	data["labels"] = [];
	data["series"] = [];
	var entrada = [];
	var salida = [];
	
	for(var i = 0; i< 24; i++){
		data["labels"][i] =  i+':00';
		entrada.push(diaObj.horaEntradas[0][i]);
		salida.push(diaObj.horaSalidas[0][i]);
	}

	data['series'] = [{
		name: "Entradas",
		data: entrada
	},
	{
		name: "Salidas",
		data: salida
	}];
						
	var options = {
		axisX: {
			showGrid: false,
			showLabel: true
		},
		lineSmooth: Chartist.Interpolation.simple({ divisor: 2 }),
	}

	new Chartist.Line('#graph-dia', data, options);
}

function graphMax(){
	var data = {};
	data["labels"] = [];
	data["series"] = [];
	
	data = {
		labels: ['Max', 'Prom'],
		series: [[diaObj.max],[diaObj.prom]]
	};

	var options = {
	  seriesBarDistance: 15
	};

	new Chartist.Bar('#graph-max', data);
}