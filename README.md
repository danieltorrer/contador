# FLUX

Flux es un contador de personas, utiliza arduino y un servidor en tiempo real basado en Node.js.

Creado para el desafío *Counting People through Intelligence of Things* de #CPMX6 

![Imgur](http://i.imgur.com/4N4ceoD.png)

![Imgur](http://i.imgur.com/vXqhhY0.png)

![Imgur](http://i.imgur.com/m0atbSQ.png)

![Imgur](http://i.imgur.com/U7BH8Nl.png)

![Imgur](http://i.imgur.com/sA0NwfM.png)
 
## Dependencias

### :rocket: Showtime 

Debes tener instalado git, node.js, npm y tu gestor de base de datos favorito.

#### :one: Instalar Sails*

```bash
$ sudo npm install sails -g
```

#### :two: Clonar el proyecto

Clonamos este repositorio

```bash
$ git clone https://gitlab.com/monkeythecoder/contador
```

Entramos al folder
```bash
$ cd contador
```
Instalamos todas las dependencias
```bash
$ npm install
```

#### :three: Lanzar la aplicación
```bash
$ sails lift
```

Ya puedes ver tu aplicación en: [http://localhost:1337](http://localhost:1377)


## Producción
Para lanzar la aplicación en modo de producción, ejecuta el comando:
```bash
$ NODE_ENV=production node app.js
```