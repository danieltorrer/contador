/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var moment = require('moment-timezone');

module.exports = {

	index: function(req, res){
		var diaMoment = moment().tz('America/Mexico_City').format("DD-MM-YYYY").toString();
		Dia.find({ dia: diaMoment})
		.exec(function(err, obj){
			if (err) {
				return res.notFound(err);
			}
			
			if (!obj){
				return res.notFound(err);
			}

			if (!obj.length) {
				Dia.create({dia: diaMoment}).exec(function(err,obj){
					return res.view({user: req.user, diaObj: [obj]});
				});
			}

			else{
				return res.view({user: req.user, diaObj: obj});
			}

		});
	}

}
